
import tensorrec
import tensorflow as tf
import argparse
import os
import sys
from scipy import sparse

FLAGS = None

def main(_):

    # Determine Result directory
    if FLAGS.result_dir[0] == '$':
        RESULT_DIR = os.environ[FLAGS.result_dir[1:]]
    else:
        RESULT_DIR = FLAGS.result_dir
    model_path = os.path.join(RESULT_DIR, "model")

    # Determine Data directory
    if FLAGS.data_dir[0] == '$':
        DATA_DIR = os.environ[FLAGS.data_dir[1:]]
    else:
        DATA_DIR = FLAGS.data_dir

    # Read data
    user_features_path = os.path.join(DATA_DIR, "user_features.npz")
    item_features_path = os.path.join(DATA_DIR, "item_features.npz")
    interactions_path  = os.path.join(DATA_DIR, "interactions.npz")

    user_features = sparse.load_npz(user_features_path)
    item_features = sparse.load_npz(item_features_path)
    interactions  = sparse.load_npz(interactions_path)

    # Build the model with default parameters
    model = tensorrec.TensorRec()

    # Fit the model
    model.fit(interactions, user_features, item_features, user_features.shape[1], item_features.shape[1], epochs=10)

    # Save the model for watson
    model.saved_model_builder(model_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # environment variable when name starts with $
    parser.add_argument('--data_dir', type=str, default='$DATA_DIR', help='Directory with data')
    parser.add_argument('--result_dir', type=str, default='$RESULT_DIR', help='Directory with results')

    FLAGS, unparsed = parser.parse_known_args()
    print("Start model training")
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
