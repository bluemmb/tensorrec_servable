#/bin/sh

zip -r compressed/recommender.zip tensorrec main.py requirements.txt setup.cfg setup.py \
    -x *__pycache__*
